# PascalGraph #

Problem:

By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

```
#!text
   3
  7 4
 2 4 6
8 5 9 3
```


That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom of the triangle below:

```
#!text
                            75
                          95  64
                        17  47  82
                      18  35  87  10
                    20  04  82  47  65
                  19  01  23  75  03  34
                88  02  77  73  07  63  67
              99  65  04  28  06  16  70  92
            41  41  26  56  83  40  80  70  33
          41  48  72  33  47  32  37  16  94  29
        53  71  44  65  25  43  91  52  97  51  14
      70  11  33  28  77  73  17  78  39  68  17  57
    91  71  52  38  17  14  91  43  58  50  27  29  48
  63  66  04  68  89  53  67  30  73  16  69  87  40  31
04  62  98  27  23  09  70  98  73  93  38  53  60  04  23
```

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

The problem is expressed as a graph with properties of a pascal triangle. It's not a tree because a child may have multiple parents and corollary, a parent may share a child with another parent. 

It is directed graph where each non-leaf node has exactly 2 child nodes. Moreover, such a directed graph follows properties akin to a pascal triangle.

-  the number of nodes/vertices on a row is similar - it is just the distance from the root node
-  and similarly, the calculation for total number of nodes in the graph/triangle - sum of vertices per row
-  the adjacency properties of the nodes from an upper tier/row to the nodes on the lower tier

The last property (adjacency property) is what defines the problem as a graph problem. Structurally though, it looks like a binary tree... but it isn't. The only properties it inherits from a tree are the following:

-  The graph has a root.
-  The nodes may be "non-leaf" or "leaf" depending if they have (exactly 2) children or none respectively.
-  A "height"

The last property is where it becomes obvious that this structure is NOT a binary tree structure. The height of a binary tree (h) defines the number of nodes of the tree such that it is equal to (2^h) - 1. The number of nodes in this pascal graph given the height (h) is equal to (i=1)_E_(h) (i).

### How do I get set up? ###

The project would require:

-  Java 8. 
-  Maven
-  Optional: Eclipse

Open the project in Eclipse. Import it as a Maven project. Run the Solver.java application passing a -file=<your data file> parameter where <your data file> corresponds to an encoding of the pascal graph. I have made some sample of this under the /data directory.