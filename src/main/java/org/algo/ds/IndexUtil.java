package org.algo.ds;

public class IndexUtil {
    public static int getSumOfNaturalNumbers(int n) {
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            sum += i;
        }
        return sum;
    }
    
    public static int toIndex(int rowIndex, int columnIndex) {
        return toIndex(new int[] {rowIndex, columnIndex});
    }
    
    public static int toIndex(int[] coordinates) {
        return getSumOfNaturalNumbers(coordinates[0]) + coordinates[1];
    }
    
    public static int[] toCoordinates(int sequenceIndex) {
        int rowIndex = 0;
        while (toIndex(rowIndex, 0) < sequenceIndex) {
            rowIndex++;
        }
        if (toIndex(rowIndex, 0) == sequenceIndex) {
            return new int[] { rowIndex, 0 };
        }
        return new int[] { rowIndex - 1, sequenceIndex - toIndex(rowIndex - 1, 0) };
    }

    public static int[][] generatePaths(int bitCount) {
        int pathCount = (int) Math.pow(2, bitCount);
        int[][] paths = new int[pathCount][bitCount];
        for (int i = 0; i < pathCount; i++) {
            paths[i] = generatePath(bitCount, i);
        }
        return paths;
    }
    
    public static int[] generatePath(int bitCount, int i) {
        int[] path = new int[bitCount];
        for (int j = 0; j < bitCount; j++) {
            path[j] = (i / ((int) Math.pow(2, j))) % 2;
        }
        return path;
    }
}
