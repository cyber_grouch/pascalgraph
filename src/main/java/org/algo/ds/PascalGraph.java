package org.algo.ds;

import java.util.Arrays;
import java.util.List;

import com.google.common.collect.Lists;

public class PascalGraph {
    
    private int[] nodeSequence;

    public PascalGraph(int[] nodeSequence) {
        this.nodeSequence = nodeSequence;
    }
    
    /**
     * Returns node given zero-based rowIndex and columnIndex
     * 
     * @param rowIndex - zero-based index for the row in the Pascal graph
     * @param columnIndex - zero-based index for the column in the Pascal graph
     * @return Node in the graph corresponding to (rowIndex, columnIndex)
     */
    public Node getNode(int rowIndex, int columnIndex) {
        return getNode(new int[] {rowIndex, columnIndex});
    }
    
    private Node getNode(int sequenceIndex) {
        return getNode(IndexUtil.toCoordinates(sequenceIndex));
    }
    
    private Node getNode(int[] coordinates) {
        int rowIndex = coordinates[0];
        int columnIndex = coordinates[1];
        if (columnIndex > rowIndex) {
            throw new IllegalArgumentException(String.format("Not a valid coordinate for a Pascal Graph: (%s,%s)", rowIndex, columnIndex));
        }
        if (columnIndex < 0) {
            throw new IllegalArgumentException(String.format("Negative column index: (%s)",columnIndex));
        }
        if (IndexUtil.toIndex(coordinates) >= nodeSequence.length) {
            throw new IllegalArgumentException(String.format("Not a valid coordinate for given Pascal Graph: (%s,%s)", rowIndex, columnIndex));
        }
        return new Node(coordinates);
    }
    
    public class Node {

        private int[] coordinates;
        
        public Node(int[] coordinates) {
            this.coordinates = coordinates;
        }

        public int getRowIndex() {
            return coordinates[0];
        }

        public int getColumnIndex() {
            return coordinates[1];
        }

        public int getSequenceIndex() {
            return IndexUtil.toIndex(coordinates);
        }

        public int getValue() {
            int index = getSequenceIndex();
            return nodeSequence[index];
        }

        public boolean isLeaf() {
            return IndexUtil.toIndex(getChildCoordinates(true)) > nodeSequence.length;
        }

        public int[] getChildCoordinates(boolean left) {
            return new int[] { getRowIndex() + 1, left ? getColumnIndex() : getColumnIndex() + 1};
        }
        
        public Node getChild(boolean left) {
            int[] childCoordinate = getChildCoordinates(left);
            return getNode(childCoordinate[0], childCoordinate[1]);
        }

        public Node[] getChildren() {
            return new Node[] { getChild(true), getChild(false) };
        }
        
        @Override
        public String toString() {
            return String.format("(%s,%s)=[%s]", this.getRowIndex(), this.getColumnIndex(), this.getValue());
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + getOuterType().hashCode();
            result = prime * result + Arrays.hashCode(coordinates);
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Node other = (Node) obj;
            if (!getOuterType().equals(other.getOuterType())) {
                return false;
            }
            if (!Arrays.equals(coordinates, other.coordinates)) {
                return false;
            }
            return true;
        }

        private PascalGraph getOuterType() {
            return PascalGraph.this;
        }
    }

    public int size() {
        return nodeSequence.length;
    }

    public Node getRoot() {
        return getNode(0, 0);
    }

    public int getHeight() {
        return getNode(nodeSequence.length - 1).getRowIndex() + 1;
    }

    public int getPathCount() {
        return (int) Math.pow(2, getHeight() - 1);
    }

    public List<Node> getPath(int pathIndex) {
        int[] path = IndexUtil.generatePath(getHeight() - 1, pathIndex);
        List<Node> nodes = Lists.newArrayList();
        Node current = getRoot();
        nodes.add(current);
        for (int j = 0; j < path.length; j++) {
            current = current.getChild(path[j] == 0);
            nodes.add(current);
        }
        return nodes;
    }
}

