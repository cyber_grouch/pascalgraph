package org.algo.ds;

import java.util.Random;
import java.util.stream.IntStream;

public class PascalGraphGenerator {
    public static PascalGraphGenerator getInstance() {
        return Holder.instance;
    }
    private static class Holder {
        static final PascalGraphGenerator instance = new PascalGraphGenerator();
    }
    private PascalGraphGenerator() {
        super();
    }
    
    public PascalGraph generateRandom(int height, int min, int max) {
        int nodeCount = IntStream.iterate(1, n -> n + 1).limit(height).sum();
        Random random = new Random();
        return new PascalGraph(IntStream.iterate(0,  n -> n + 1)
                                        .limit(nodeCount)
                                        .map(x -> random.nextInt(max - min + 1) + min)
                                        .toArray());
    }
}
