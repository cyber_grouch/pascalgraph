package org.algo.graph;

import java.util.List;
import java.util.stream.IntStream;

import org.algo.ds.PascalGraph;
import org.apache.commons.lang3.tuple.Pair;

public class ParallelBruteForceStrategy  implements Strategy<PascalGraph, Pair<Integer, List<PascalGraph.Node>>> {

    @Override
    public Pair<Integer, List<PascalGraph.Node>> solve(PascalGraph graph) {
        System.out.println("Strategy: ParallelBruteForceStrategy");
        int numberOfPathsFromRootToLeaf = graph.getPathCount();
        return IntStream.iterate(0, n -> n + 1)
                        .limit(numberOfPathsFromRootToLeaf).parallel()
                        .mapToObj(i -> graph.getPath(i))
                        .map(p -> Pair.of(p.parallelStream()
                                           .mapToInt(a -> a.getValue())
                                           .sum(),
                                          p))
                        .max((x, y) -> x.getLeft() - y.getLeft())
                        .get();
    }

}
