package org.algo.graph;

import java.util.Collections;
import java.util.List;

import org.algo.ds.PascalGraph;
import org.algo.ds.PascalGraph.Node;
import org.apache.commons.lang3.tuple.Pair;

public class SequentialBruteForceStrategy implements Strategy<PascalGraph, Pair<Integer, List<PascalGraph.Node>> > {

    @Override
    public Pair<Integer, List<PascalGraph.Node>>  solve(PascalGraph graph) {
        System.out.println("Strategy: SequentialBruteForceStrategy");
        int numberOfPathsFromRootToLeaf = graph.getPathCount();
        int maxValue = -1;
        List<Node> maxValuePath = Collections.<Node>emptyList();
        for(int i = 0; i < numberOfPathsFromRootToLeaf; i++) {
            List<Node> path = graph.getPath(i);
            int value = path.stream().mapToInt(n -> n.getValue()).sum();
            int newMaxValue = Math.max(maxValue, value);
            if (maxValue != newMaxValue) {
                maxValuePath = path;
                maxValue = newMaxValue;
            }
        }
        return Pair.of(maxValue, maxValuePath);
    }

}
