package org.algo.graph;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.algo.ds.PascalGraph;
import org.algo.ds.PascalGraphGenerator;
import org.algo.ds.PascalGraph.Node;
import org.apache.commons.lang3.tuple.Pair;

import com.google.common.collect.Lists;

public class Solver {
    
    /**
     * The main class
     */
    public static void main(String[] args) {
        try {
            solve(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Read the instance, solve it, and print the solution in the standard output
]     * @throws Exception 
     */
    public static void solve(String[] args) throws Exception {

        
        List<String> strategies = Lists.newArrayList(
           "org.algo.graph.SequentialBruteForceStrategy",
           "org.algo.graph.ParallelBruteForceStrategy"
        );
        
        PascalGraph graph = generateViaGenerator(24);
        strategies.stream().forEach(s -> applyStrategy(s, graph));
    }
    
    @SuppressWarnings("unchecked")
    public static void applyStrategy(String strategyClass, PascalGraph graph) {
        Strategy<PascalGraph, Pair<Integer, List<PascalGraph.Node>>> s;
        try {
            s = (Strategy<PascalGraph,
                         Pair<Integer,
                              List<Node>>>) Class.forName(strategyClass).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        
        Timer timer = Timer.createInstance();
        timer.start();
        Pair<Integer, List<PascalGraph.Node>> solution = s.solve(graph);
        long v = timer.stop();
        System.out.println(String.format("Graph:: height = %s  ;  nodes = %s", graph.getHeight(), graph.size()));
        System.out.println(String.format("time (ns) = %s", v));
        System.out.println(String.format("maxValue = %s", solution.getLeft()));
        System.out.println(Arrays.toString(solution.getRight().stream().map(x -> x.toString()).toArray()));
    }
    
    public static PascalGraph generateViaGenerator(int height) {
        return PascalGraphGenerator.getInstance().generateRandom(height, 1, 10000);
    }
    
    public static PascalGraph generateFromFile(String[] args) throws FileNotFoundException, IOException {
        String fileName = null;
        
        // get the temp file name
        for(String arg : args){
            if(arg.startsWith("-file=")){
                fileName = arg.substring(6);
            } 
        }
        if(fileName == null) {
            System.exit(-1);
        }
        
        List<Integer> intInput = new LinkedList<Integer>();
        try (BufferedReader input =  new BufferedReader(new FileReader(fileName))) {
            String line = null;
            while ((line = input.readLine()) != null) {
                String[] inputSplit = line.trim().split("\\s+");
                for (int i = 0; i < inputSplit.length; i++) {
                    String stringInput = inputSplit[i].trim();
                    if (!stringInput.isEmpty()) {
                        intInput.add(Integer.parseInt(stringInput));
                    }
                }
            }
        }
        int[] intInputArr = intInput.stream().mapToInt(x -> x.intValue()).toArray();
        return new PascalGraph(intInputArr);
    }
}
