package org.algo.graph;

public interface Strategy<K, T> {
    T solve(K input);
}
