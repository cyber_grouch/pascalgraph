package org.algo.graph;

public class Timer {
    private Timer() {
        super();
    }
    
    public static Timer createInstance() {
        Timer timer = new Timer();
        return timer;
    }
    
    long start = 0;
    
    public void start() {
        start = System.nanoTime();
    }
    
    public long stop() {
        return System.nanoTime() - start;
    }
    
}
