package org.algo.ds;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

public class IndexUtilTest {

    @Test
    public void testCoordinatesToSequenceIndex() {
        assertEquals(3, IndexUtil.toIndex(2, 0));
        assertEquals(4, IndexUtil.toIndex(2, 1));
        assertEquals(5, IndexUtil.toIndex(2, 2));
    }
    
    public void testSumOfNaturalNumbers() {
        assertEquals(0, IndexUtil.getSumOfNaturalNumbers(0));
        assertEquals(1, IndexUtil.getSumOfNaturalNumbers(1));
        assertEquals(3, IndexUtil.getSumOfNaturalNumbers(2));
        assertEquals(6, IndexUtil.getSumOfNaturalNumbers(3));
        assertEquals(10, IndexUtil.getSumOfNaturalNumbers(4));
        assertEquals(15, IndexUtil.getSumOfNaturalNumbers(5));
        assertEquals(21, IndexUtil.getSumOfNaturalNumbers(6));
        assertEquals(28, IndexUtil.getSumOfNaturalNumbers(7));
        assertEquals(36, IndexUtil.getSumOfNaturalNumbers(8));
        assertEquals(45, IndexUtil.getSumOfNaturalNumbers(9));
        assertEquals(55, IndexUtil.getSumOfNaturalNumbers(10));
    }
    
    @Test
    public void testSequenceIndexToCoordinates() {
        assertTrue(intArrayEquals(new int[] { 0, 0 }, IndexUtil.toCoordinates(0)));
        assertTrue(intArrayEquals(new int[] { 1, 0 }, IndexUtil.toCoordinates(1)));
        assertTrue(intArrayEquals(new int[] { 1, 1 }, IndexUtil.toCoordinates(2)));
        assertTrue(intArrayEquals(new int[] { 2, 0 }, IndexUtil.toCoordinates(3)));
        assertTrue(intArrayEquals(new int[] { 2, 1 }, IndexUtil.toCoordinates(4)));
        assertTrue(intArrayEquals(new int[] { 2, 2 }, IndexUtil.toCoordinates(5)));
        assertTrue(intArrayEquals(new int[] { 3, 0 }, IndexUtil.toCoordinates(6)));
        assertTrue(intArrayEquals(new int[] { 3, 1 }, IndexUtil.toCoordinates(7)));
        assertTrue(intArrayEquals(new int[] { 3, 2 }, IndexUtil.toCoordinates(8)));
        assertTrue(intArrayEquals(new int[] { 3, 3 }, IndexUtil.toCoordinates(9)));
        assertTrue(intArrayEquals(new int[] { 4, 0 }, IndexUtil.toCoordinates(10)));
    }
    
    @Test
    public void testGeneratePaths() {
        int[][] paths = IndexUtil.generatePaths(3);
        assertEquals(8, paths.length);
        assertEquals(3, paths[0].length);
    }
    
    private boolean intArrayEquals(int[] arr1, int[] arr2) {
        return Arrays.equals(arr1, arr2);
    }


}

