package org.algo.ds;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PascalGraphTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    private PascalGraph pg;
    private int[] seq;
    @Before
    public void setUp() throws Exception {
        seq = new int[] {
                           3,
                          7, 4,
                        2, 4, 6,
                       8, 5, 9, 3
        };
        pg = new PascalGraph(seq);
    }

    @After
    public void tearDown() throws Exception {
    }
    

    @Test
    public void testInitialization() {
        PascalGraph graph = new PascalGraph(new int[] {1, 1, 1, 1, 2, 1 });
        assertNotNull(graph);
        assertEquals(6, graph.size());
        assertEquals(3, graph.getHeight());
    }
    
    @Test
    public void testRootNode() {
        assertEquals(4, pg.getHeight());
        PascalGraph.Node node = pg.getRoot();
        assertNotNull(node);
        assertEquals(0, node.getRowIndex());
        assertEquals(0, node.getColumnIndex());
        assertEquals(0, node.getSequenceIndex());
        assertEquals(3, node.getValue());
        assertFalse(node.isLeaf());
    }

    
    @Test
    public void testNodeGeneration() {
        PascalGraph.Node node = pg.getNode(2, 2);
        assertNotNull(node);
        assertEquals(2, node.getRowIndex());
        assertEquals(2, node.getColumnIndex());
        assertEquals(5, node.getSequenceIndex());
        assertEquals(6, node.getValue());
        assertFalse(node.isLeaf());
        
        PascalGraph.Node leftChild = node.getChild(true);
        assertNotNull(leftChild);
        assertEquals(3, leftChild.getRowIndex());
        assertEquals(2, leftChild.getColumnIndex());
        assertEquals(8, leftChild.getSequenceIndex());
        assertEquals(9, leftChild.getValue());
        assertTrue(leftChild.isLeaf());

        PascalGraph.Node rightChild = node.getChild(false);
        assertNotNull(rightChild);
        assertEquals(3, rightChild.getRowIndex());
        assertEquals(3, rightChild.getColumnIndex());
        assertEquals(9, rightChild.getSequenceIndex());
        assertEquals(3, rightChild.getValue());
        assertTrue(rightChild.isLeaf());
        
        PascalGraph.Node[] children = node.getChildren();
        assertEquals(leftChild, children[0]);
        assertEquals(rightChild, children[1]);
    }
    
    @Test
    public void testNodeGenerationLeaf() {
        PascalGraph.Node node = pg.getNode(3, 2);
        assertNotNull(node);
        assertEquals(3, node.getRowIndex());
        assertEquals(2, node.getColumnIndex());
        assertEquals(8, node.getSequenceIndex());
        assertEquals(9, node.getValue());
        assertTrue(node.isLeaf());
    }

    
    @Test(expected=IllegalArgumentException.class)
    public void testNodeGenerationInvalidColumnIndex() {
        pg.getNode(3, 4);
        fail("Did not throw an IllegalArgumentException for invalid column index coordinate.");
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testNodeGenerationInvalidNegativeColumnIndex() {
        pg.getNode(0, -1);
        fail("Did not throw an IllegalArgumentException for negative index.");
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testNodeGenerationInvalidNodeOutOfRange() {
        pg.getNode(10, 10);
        fail("Did not throw an IllegalArgumentException for node range exceeded case.");
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testNodeGenerationFromLeafNode() {
        pg.getNode(3, 3).getChild(true);
        fail("Did not throw an IllegalArgumentException for node generation from leaf.");
    }
    
    @Test
    public void testPathCount() {
        assertEquals(8, pg.getPathCount());
    }
    
    @Test
    public void testPathGeneration() {
        List<PascalGraph.Node> path = pg.getPath(3);
        assertNotNull(path);
        assertEquals(4, path.size());
        
        PascalGraph.Node root = path.get(0);
        assertEquals(0, root.getRowIndex());
        assertEquals(0, root.getColumnIndex());
        assertFalse(root.isLeaf());
        
        PascalGraph.Node leaf = path.get(3);
        assertEquals(3, leaf.getRowIndex());
        assertTrue(leaf.isLeaf());
        
    }
}
